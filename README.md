# Capbay Mamak Checkout System

## Table of Contents

1. [Demo](#demo)
    - [How to run](#how-to-run)
    - [Configuring demo with different data sets](#configuring-demo-with-different-data-sets)
2. [Interface](#interface)
    - [Basic usage](#basic-usage)
    - [Checkout system](#checkout-system)
    - [Pricing rules](#pricing-rules)
    - [Scannable items and item creation](#scannable-items-and-item-creation)
    - [Design](#design)
4. [Tests](#tests)
    - [How to run](#how-to-run-1)

## Demo

The demo shows the first test case in a table form.

### How to run
---
1. Install [PHP](https://www.php.net/) and add to PATH.
2. Run `php artisan serve` at repository root.
3. Go to `http://127.0.0.1:8000` on any web browser.

### Configuring demo with different data sets
---
The demo can be configured to display different tables corresponding to different data sets. Configuration can be done at [demo.blade.php](resources/views/demo.blade.php) at the following section:

```
...

// Create items to scan.
$items = [
    KopiFactory::create(),
    RotiKosongFactory::create(),
    TehTarikFactory::create(),
    KopiFactory::create(),
    RotiKosongFactory::create(),
];

// Create pricing rules.
$pricingRules = new CompositeRule();
$pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
$pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));
$pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

...
```

## Interface

### Basic usage
---
```
// Create items to scan.
$items = [
    KopiFactory::create(),
    RotiKosongFactory::create(),
    TehTarikFactory::create(),
];

// Create pricing rules.
$pricingRules = new CompositeRule();
$pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
$pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

// Do checkout.
$checkout = new Checkout($pricingRules);
$checkout->scan($items[0]);
$checkout->scan($items[1]);
$checkout->scan($items[2]);
$totalPrice = $checkout->total();
```

### Checkout system
---
The checkout system can be given a set of pricing rules. A pricing rule needs to implement [IPricingRule](app/CheckoutSystem/IPricingRule.php) while a scanned item needs to implement [ICheckoutItem](app/CheckoutSystem/ICheckoutItem.php).
The total price after applying the pricing rules can be obtained.
```
$checkout = new Checkout($pricingRules);
$checkout->scan($item);
$checkout->scan($item);
$totalPrice = $checkout->total();
```
A transformed item list can also be obtained. The transformed item list contains information about the new prices for the items as well as the rules applied on the items. This is used in the demo to display the transformations in table format. See the [Demo](#demo) and [Checkout.php](app/CheckoutSystem/Checkout.php) for more info.
```
$transformedItemList = $checkout->getTransItemList();
```

### Pricing rules
---
Pricing rules can be defined like so:

```
// BuyOneFreeOneRule implements IPricingRule
$pricingRules = new BuyOneFreeOneRule(...);
```
Pricing rules can also be defined compositely with [CompositeRule](app/CheckoutSystem/CompositeRule.php). When defined this way, the pricing rules are applied sequentially. This allows a quick and easy way to define complex pricing rules.
```
$pricingRules1 = new CompositeRule();
$pricingRules1->add(new BuyOneFreeOneRule(...)); // Applied first.
$pricingRules1->add(new MassDiscountRule(...)); // Applied second.

$pricingRules2 = new CompositeRule();
$pricingRules2->add(new BuyOneFreeOneRule(...)); // Applied third.
$pricingRules2->add(new MassDiscountRule(...)); // Applied fourth.

// We can even combine 2 composite rules.
$pricingRules = new CompositeRule();
$pricingRules->add($pricingRules1);
$pricingRules->add($pricingRules2);
```
Note that the time complexity increases with the complexity of the composition. For compositions that require low time complexity, extending via [IPricingRule](app/CheckoutSystem/IPricingRule.php) is highly recommended. Extending new pricing rules can be done like so:
```
class MyPricingRule implements IPricingRule {
    ...
}
```

### Scannable items and item creation
---
An item must implement [ICheckoutItem](app/CheckoutSystem/ICheckoutItem.php) to be considered "scannable" by the checkout system. A simple [Item](app/CheckoutSystem/Item.php) class is included and its creation can be done via the factory methods like so:
```
$item1 = KopiFactory::create()
$item2 = TehTarikFactory::create()
$item3 = RotiKosongFactory::create()
```
New item types can be added by extending [ItemFactory](app/CheckoutSystem/ItemFactory.php):
```
class MiloFactory extends ItemFactory {
    ...
}
```
The item and item factories are implemented for demo purposes only. It can be replaced entirely as long as the items to be scanned implement [ICheckoutItem](app/CheckoutSystem/ICheckoutItem.php).

### Design
---
![class-diagram](images/class-diagram.png)

## Tests

### How to run
---
1. Install [PHP](https://www.php.net/) and add to PATH.
2. Run `php artisan test` at repository root.