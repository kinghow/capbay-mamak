<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\IDisplayable;
use App\CheckoutSystem\IPricingRule;

/**
 * Pricing rule for buy 1, free 1. The free pricing is always applied
 * on the even-numbered item.
 */
class BuyOneFreeOneRule implements IPricingRule, IDisplayable {
    private string $itemCode = "";

    public function __construct(string $itemCode) {
        $this->itemCode = $itemCode;
    }

    public function apply(array &$itemList) {
        $count = 1;
        foreach ($itemList as &$itemPricePair) {
            if ($itemPricePair[0]->getCode() === $this->itemCode) {
                if ($count % 2 === 0) {
                    $itemPricePair[1] = 0.0;
                    $itemPricePair[2][] = $this->getDesc();
                }
                $count++;
            }
        }
    }

    public function getDesc() : string {
        return "Buy 1, Free 1";
    }
}