<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\IPricingRule;
use App\CheckoutSystem\CompositeRule;
use App\CheckoutSystem\ICheckoutItem;

/**
 * Represents the checkout system. Given a pricing rule, the total price of
 * scanned items can be obtained with respect to the pricing rule.
 */
class Checkout {
    private IPricingRule $rule;
    private array $itemList = [];

    /**
     * Constructs a checkout system with no pricing rules by default.
     */
    public function __construct(IPricingRule $pricingRule = new CompositeRule()) {
        $this->rule = $pricingRule;
    }

    public function scan(ICheckoutItem $item) {
        $this->itemList[] = [$item, $item->getPrice(), []];
    }

    /**
     * Returns the total price after transformations from the pricing rules.
     */
    public function total() : float {
        $transItemList = $this->getTransItemList();

        $totalPrice = 0.0;
        foreach ($transItemList as [$item, $price]) {
            $totalPrice += $price;
        }

        return $totalPrice;
    }

    /**
     * Returns an array of item list with the transformations from the
     * pricing rules applied. Returned array is an indexed array with the
     * following structure:
     * [0] => [$item, $transformedPrice, [($ruleDescriptions)]]
     */
    public function getTransItemList() : array {
        $transItemList = $this->itemList;
        $this->rule->apply($transItemList);
        return $transItemList;
    }
}
