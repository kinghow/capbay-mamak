<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\IPricingRule;

/**
 * Allows the composition of pricing rules for a more complex pricing rule.
 * Note that the time complexity increases with the complexity of the composition.
 * In that case, a custom implementation of IPricingRule is highly recommended.
 * 
 * Use this class for an empty rule.
 */
class CompositeRule implements IPricingRule {
    private $ruleList = [];

    public function __construct() {}

    public function add(IPricingRule $rule) {
        $this->ruleList[] = $rule;
    }

    public function apply(array &$itemList) {
        foreach ($this->ruleList as $rule) {
            $rule->apply($itemList);
        }
    }
}