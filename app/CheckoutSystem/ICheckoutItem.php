<?php

namespace App\CheckoutSystem;

/**
 * This interface is required for item classes to interface with this CheckoutSystem.
 * An item class that implements this interface can be thought of as "scannable" by
 * this CheckoutSystem.
 */
interface ICheckoutItem {
    public function getCode() : string;
    public function getPrice() : float;
}
