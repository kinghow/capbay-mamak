<?php

namespace App\CheckoutSystem;

/**
 * Utility interface to allow an object to express itself as a string.
 */
interface IDisplayable {
    public function getDesc();
}
