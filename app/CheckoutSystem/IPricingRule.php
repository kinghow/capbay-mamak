<?php

namespace App\CheckoutSystem;

/**
 * Interface for a pricing rule.
 */
interface IPricingRule {
    /**
     * Applies the price transformations on the array incurred by this rule.
     * The array should be an indexed array with the following structure:
     * [0] => [$item, $price, []]
     * This method may modify the array passed in.
     */
    public function apply(array &$itemList);
}