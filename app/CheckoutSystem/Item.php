<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\ICheckoutItem;

/**
 * A sample item class to represent an item to be used with this CheckoutSystem.
 * An item class may look different in another context as long as it implements
 * ICheckoutItem to be able to interface with this CheckoutSystem.
 */
class Item implements ICheckoutItem {
    private string $code = "";
    private string $name = "";
    private float $price = 0.0;

    public function __construct(string $code, string $name, float $price) {
        $this->code = $code;
        $this->name = $name;
        $this->price = $price;
    }

    public function getCode() : string {
        return $this->code;
    }

    public function getName() : string {
        return $this->name;
    }

    public function getPrice() : float {
        return $this->price;
    }
}
