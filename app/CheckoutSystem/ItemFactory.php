<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\Item;

/**
 * A class for mass producing items. Only for demo.
 */
abstract class ItemFactory {
    abstract public static function create() : Item;
    abstract public static function getCode() : string;
}
