<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\Item;
use App\CheckoutSystem\ItemFactory;

/**
 * Mass production for kopi. Only for demo.
 */
class KopiFactory extends ItemFactory {
    public static function create() : Item {
        return new Item(KopiFactory::getCode(), "Kopi", 2.5);
    }

    public static function getCode() : string {
        return "B001";
    }
}
