<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\IDisplayable;
use App\CheckoutSystem\IPricingRule;

/**
 * Pricing rule for mass discount. When the number of item is at $threshold or more,
 * the $newPrice will take effect for every item of that type.
 */
class MassDiscountRule implements IPricingRule, IDisplayable {
    private string $itemCode = "";
    private int $threshold = 0;
    private float $newPrice = 0.0;

    public function __construct(string $itemCode, int $threshold, float $newPrice) {
        $this->itemCode = $itemCode;
        $this->threshold = $threshold;
        $this->newPrice = $newPrice;
    }

    public function apply(array &$itemList) {
        $transRef = [];

        foreach ($itemList as $idx => [$item, $price]) {
            if ($item->getCode() === $this->itemCode) {
                $transRef[] = $idx;
            }
        }

        if (count($transRef) >= $this->threshold) {
            foreach ($transRef as $idx) {
                $itemList[$idx][1] = $this->newPrice;
                $itemList[$idx][2][] = $this->getDesc();
            }
        }
    }

    public function getDesc() : string {
        return "Buy " . $this->threshold . " or more, get " . number_format((float)$this->newPrice, 2, '.', '') . " each";
    }
}