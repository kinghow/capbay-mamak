<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\Item;
use App\CheckoutSystem\ItemFactory;

/**
 * Mass production for roti kosong. Only for demo.
 */
class RotiKosongFactory extends ItemFactory {
    public static function create() : Item {
        return new Item(RotiKosongFactory::getCode(), "Roti Kosong", 1.5);
    }

    public static function getCode() : string {
        return "F001";
    }
}
