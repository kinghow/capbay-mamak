<?php

namespace App\CheckoutSystem;

use App\CheckoutSystem\Item;
use App\CheckoutSystem\ItemFactory;

/**
 * Mass production for teh tarik. Only for demo.
 */
class TehTarikFactory extends ItemFactory {
    public static function create() : Item {
        return new Item(TehTarikFactory::getCode(), "Teh Tarik", 2.0);
    }

    public static function getCode() : string {
        return "B002";
    }
}
