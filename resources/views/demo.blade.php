<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout System</title>
</head>
<body>

    <h1>Checkout System</h1>

    @php
        use App\CheckoutSystem\Item;
        use App\CheckoutSystem\Checkout;
        use App\CheckoutSystem\DefaultRule;
        use App\CheckoutSystem\PricingRule;
        use App\CheckoutSystem\CompositeRule;
        use App\CheckoutSystem\MassDiscountRule;
        use App\CheckoutSystem\BuyOneFreeOneRule;
        use App\CheckoutSystem\KopiFactory;
        use App\CheckoutSystem\RotiKosongFactory;
        use App\CheckoutSystem\TehTarikFactory;

        // Create items to scan.
        $items = [
            KopiFactory::create(),
            RotiKosongFactory::create(),
            TehTarikFactory::create(),
            KopiFactory::create(),
            RotiKosongFactory::create(),
        ];

        // Create pricing rules.
        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));
        $pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

        // Do checkout.
        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $transItemList = $checkout->getTransItemList();
    @endphp

    {{-- Output checkout details in a table. --}}
    <style>
        td, th {
            border:1px solid black;
        }
        .price-data {
            text-align: right;
        }
    </style>
    <table cellpadding="10px" cellspacing="10px">
        <th>Code</th>
        <th>Item</th>
        <th>RM</th>
        @foreach ($transItemList as [$item, $price, $rules])
        <tr>
            <td>{{ $item->getCode() }}</td>
            <td>{{ $item->getName() }}</td>
            @if ($rules)
                <td class="price-data">
                    <s>{{ number_format((float)$item->getPrice(), 2, '.', '') }}</s> 
                    <span style="color:red;">{{ number_format((float)$price, 2, '.', '') }}</span>
                </td>
            @else
                <td class="price-data">{{ number_format((float)$price, 2, '.', '') }}</td>
            @endif
            @foreach ($rules as $rule)
                <td>Rule: {{ $rule }}</td>
            @endforeach
        </tr>
        @endforeach
        <tr>
            <td colspan="2"><b>Total Price</b></td>
            <td class="price-data"><b>{{ number_format((float)$totalPrice, 2, '.', '') }}</b></td>
        </tr>
    </table>

</body>
</html>