<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\CheckoutSystem\Checkout;
use App\CheckoutSystem\KopiFactory;
use App\CheckoutSystem\CompositeRule;
use App\CheckoutSystem\TehTarikFactory;
use App\CheckoutSystem\MassDiscountRule;
use App\CheckoutSystem\BuyOneFreeOneRule;
use App\CheckoutSystem\RotiKosongFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckoutTest extends TestCase
{
    /**
     * Test scenario 1 from case study.
     */
    public function test_case_study_scenario_1() {
        $items = [
            KopiFactory::create(),
            RotiKosongFactory::create(),
            TehTarikFactory::create(),
            KopiFactory::create(),
            RotiKosongFactory::create(),
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));
        $pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(6.9, $totalPrice, 0.001);
    }

    /**
     * Test scenario 2 from case study.
     */
    public function test_case_study_scenario_2() {
        $items = [
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            RotiKosongFactory::create(),
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));
        $pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(3.5, $totalPrice, 0.001);
    }

    /**
     * Test scenario 3 from case study.
     */
    public function test_case_study_scenario_3() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            TehTarikFactory::create(),
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));
        $pricingRules->add(new MassDiscountRule(RotiKosongFactory::getCode(), 2, 1.2));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(4.5, $totalPrice, 0.001);
    }

    /**
     * Checkout without any items scanned should return 0.
     */
    public function test_empty_checkout() {
        $checkout = new Checkout();
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(0.0, $totalPrice, 0.001);
    }

    /**
     * Checkout without any pricing rules should return sum of the prices of
     * all items.
     */
    public function test_no_pricing_rules() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            RotiKosongFactory::create(),
            TehTarikFactory::create(),
            RotiKosongFactory::create(),
            TehTarikFactory::create(),
        ];

        $checkout = new Checkout();
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $priceWithoutRules = 0.0;
        foreach ($items as $item) {
            $priceWithoutRules += $item->getPrice();
        }

        $this->assertEqualsWithDelta($priceWithoutRules, $totalPrice, 0.001);
    }

    /**
     * Ordered scan should return correct final price.
     */
    public function test_ordered_scan() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            RotiKosongFactory::create(),
            RotiKosongFactory::create(),
            RotiKosongFactory::create(),
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(15.5, $totalPrice, 0.001);
    }

    /**
     * Unordered scan should return correct final price.
     */
    public function test_unordered_scan() {
        $items = [
            RotiKosongFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            KopiFactory::create(),
            TehTarikFactory::create(),
            RotiKosongFactory::create(),
            TehTarikFactory::create(),
            TehTarikFactory::create(),
            RotiKosongFactory::create(),
            KopiFactory::create(),
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));
        $pricingRules->add(new BuyOneFreeOneRule(TehTarikFactory::getCode()));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(15.5, $totalPrice, 0.001);
    }

    /**
     * Buy 1, free 1 with even number of items.
     */
    public function test_buy_one_free_one_even_items() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
        ];

        $pricingRules = new BuyOneFreeOneRule(KopiFactory::getCode());

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(5.0, $totalPrice, 0.001);
    }

    /**
     * Buy 1, free 1 with odd number of items.
     */
    public function test_buy_one_free_one_odd_items() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
        ];

        $pricingRules = new BuyOneFreeOneRule(KopiFactory::getCode());

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(7.5, $totalPrice, 0.001);
    }

    /**
     * There should be no mass discount if threshold is not reached.
     */
    public function test_mass_discount_threshold_not_reached() {
        $items = [
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
            KopiFactory::create(),
        ];

        $threshold = 6;

        $pricingRules = new MassDiscountRule(KopiFactory::getCode(), $threshold, 1.0);

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(12.5, $totalPrice, 0.001);
    }

    /**
     * Additional pricing rules on the same item type should be applied
     * sequentially.
     */
    public function test_two_pricing_rules_on_same_item_type() {
        $items = [
            KopiFactory::create(), // RM 2.00
            KopiFactory::create(), // RM 0.00
            KopiFactory::create(), // RM 2.00
            KopiFactory::create(), // RM 0.00
            KopiFactory::create(), // RM 2.00
        ];

        $pricingRules = new CompositeRule();
        $pricingRules->add(new MassDiscountRule(KopiFactory::getCode(), 2, 2.0));
        $pricingRules->add(new BuyOneFreeOneRule(KopiFactory::getCode()));

        $checkout = new Checkout($pricingRules);
        foreach ($items as $item) {
            $checkout->scan($item);
        }
        $totalPrice = $checkout->total();

        $this->assertEqualsWithDelta(6.0, $totalPrice, 0.001);
    }
}
